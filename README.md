A small compiler written in Rust. It consists of a lexer, parser, intermediate code generator and a virtual machine.

# EBNF Grammar

```
 <program> ::= <statement>
 <statement> ::= "if" <paren_expr> <statement> |
                 "if" <paren_expr> <statement> "else" <statement> |
                 "while" <paren_expr> <statement> |
                 "do" <statement> "while" <paren_expr> ";" |
                 "{" { <statement> } "}" |
                 <expr> ";" |
                 ";"
 <paren_expr> ::= "(" <expr> ")"
 <expr> ::= <test> | <id> "=" <expr>
 <test> ::= <sum> | <sum> "<" <sum>
 <sum> ::= <term> | <sum> "+" <term> | <sum> "-" <term>
 <term> ::= <id> | <int> | <paren_expr>
 <id> ::= "a" | "b" | "c" | "d" | ... | "z"
 <int> ::= <an_unsigned_decimal_integer>
```

# Presentation

<https://slides.com/twinklelittle/code>
