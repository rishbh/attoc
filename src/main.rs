use std::io::{self, Read};
use std::iter::Peekable;
use std::collections::HashMap;

#[derive(Debug)]
enum Token<'a> {
	DoSym,
	ElseSym,
	IfSym,
	WhileSym,
	LBrace,
	RBrace,
	LParen,
	RParen,
	Plus,
	Minus,
	Less,
	Semi,
	EqualTo,
	Identifier(&'a str),
	Int(i32),
}

struct Tokens<'a> {
	buf: &'a str,
	c_iter: Peekable<std::str::CharIndices<'a>>,
}

impl<'a> Tokens<'a> {
	fn new(buf: &'a str) -> Self {
		Self {
			buf: buf,
			c_iter: buf.char_indices().peekable(),
		}
	}

	fn keyword(token: &'a str) -> Option<Token<'a>> {
		match token {
			"if" => Some(Token::IfSym),
			"do" => Some(Token::DoSym),
			"else" => Some(Token::ElseSym),
			"while" => Some(Token::WhileSym),
			_ => None,
		}
	}
}

impl<'a> Iterator for Tokens<'a> {
	type Item = Token<'a>;

	fn next(&mut self) -> Option<Self::Item> {
		loop {
			let (i, ch) = if let Some((i, ch)) = self.c_iter.peek() { (*i, *ch) } else { return None };
			match ch {
				'{' => { self.c_iter.next(); return Some(Token::LBrace) },
				'}' => { self.c_iter.next(); return Some(Token::RBrace) },
				'(' => { self.c_iter.next(); return Some(Token::LParen) },
				')' => { self.c_iter.next(); return Some(Token::RParen) },
				'+' => { self.c_iter.next(); return Some(Token::Plus) },
				'-' => { self.c_iter.next(); return Some(Token::Minus) },
				'<' => { self.c_iter.next(); return Some(Token::Less) },
				';' => { self.c_iter.next(); return Some(Token::Semi) },
				'=' => { self.c_iter.next(); return Some(Token::EqualTo) },
				'A'..='Z' | 'a'..='z' => {
					let j = i;
					loop {
						if let Some((i, ch)) = self.c_iter.peek() {
							if !ch.is_ascii_alphabetic() {
								if let Some(k) = Self::keyword(&self.buf[j..*i]) {
									return Some(k);
								} else {
									return Some(Token::Identifier(&self.buf[j..*i]));
								}
							}
						} else {
							if let Some(k) = Self::keyword(&self.buf[j..]) {
								return Some(k);
							} else {
								return Some(Token::Identifier(&self.buf[j..]));
							}
						}
						self.c_iter.next();
					}
				},
				'0'..='9' => {
					let j = i;
					loop {
						if let Some((i, ch)) = self.c_iter.peek() {
							if !ch.is_ascii_digit() {
								return Some(Token::Int((&self.buf[j..*i]).parse().unwrap()));
							}
						} else {
							return Some(Token::Int((&self.buf[j..]).parse().unwrap()));
						}
						self.c_iter.next();
					}
				},
				' ' | '\n' => { self.c_iter.next(); }
				'\0' => return None,
				_ => panic!("Unhandled Symbol Type!"),
			}
		}
	}
}

#[derive(Debug)]
enum Node<'a> {
	Var(&'a str),
	Cst(i32),
	Add{o1: Box<Node<'a>>, o2: Box<Node<'a>>},
	Sub{o1: Box<Node<'a>>, o2: Box<Node<'a>>},
	Lt{o1: Box<Node<'a>>, o2: Box<Node<'a>>},
	Set{o1: Box<Node<'a>>, o2: Box<Node<'a>>},
	If1{o1: Box<Node<'a>>, o2: Box<Node<'a>>},
	If2{o1: Box<Node<'a>>, o2: Box<Node<'a>>, o3: Box<Node<'a>>},
	While{o1: Box<Node<'a>>, o2: Box<Node<'a>>},
	Do{o1: Box<Node<'a>>, o2: Box<Node<'a>>},
	Empty,
	Seq{o1: Box<Node<'a>>, o2: Box<Node<'a>>},
	Expr{o1: Box<Node<'a>>},
	Prog{o1: Box<Node<'a>>},
}

struct Parser<'a> {
	token_iter: Peekable<Tokens<'a>>,
}

impl<'a> Parser<'a> {
	fn new(buf: &'a str) -> Self {
		Self {
			token_iter: Tokens::new(buf).peekable(),
		}
	}

	fn program<'f>(&'f mut self) -> Box<Node<'a>> {
		Box::new(Node::Prog{o1: self.statement()})
	}

	fn statement<'f>(&'f mut self) -> Box<Node<'a>> {
		let mut t: Box<Node>;
		let mut x: Box<Node>;
		match self.token_iter.peek().unwrap() {
			Token::IfSym => {
				self.token_iter.next();
				let o1 = self.paren_exp();
				let o2 = self.statement();
				
				if let Some(Token::ElseSym) = self.token_iter.peek() {
					// change kind of x
					x = Box::new(Node::If2{o1: o1, o2: o2, o3: self.statement()});
				} else {
					x = Box::new(Node::If1{o1: o1, o2: o2});
				}
			},

			Token::WhileSym => {
				self.token_iter.next();
				x = Box::new(Node::While{o1: self.paren_exp(), o2: self.statement()});
			},

			Token::DoSym => {
				self.token_iter.next();
				let o1 = self.statement();
				if let Token::WhileSym = self.token_iter.peek().unwrap() { self.token_iter.next(); } else { panic!("syntax error"); }
				let o2 = self.paren_exp();
				if let Token::Semi = self.token_iter.peek().unwrap() { self.token_iter.next(); } else { panic!("syntax error"); }
				x = Box::new(Node::Do{o1: o1, o2: o2});
			},

			Token::Semi => {
				x = Box::new(Node::Empty);
				self.token_iter.next();
			},

			Token::LBrace => {
				x = Box::new(Node::Empty);
				self.token_iter.next();
				loop {
					if let Token::RBrace = self.token_iter.peek().unwrap() { break; }
					t = x;
					x = Box::new(Node::Seq{o1: t, o2: self.statement()});
				}
				self.token_iter.next();
			}

			_ => {
				x = Box::new(Node::Expr{o1: self.expr()});
				if let Token::Semi = self.token_iter.peek().unwrap() { self.token_iter.next(); } else { panic!("syntax error"); }
			},
		}
		return x;
	}

	fn paren_exp<'f>(&'f mut self) -> Box<Node<'a>> {
		if let Token::LParen = self.token_iter.peek().unwrap() { self.token_iter.next(); } else { panic!("syntax error"); }
		let x = self.expr();
		if let Token::RParen = self.token_iter.peek().unwrap() { self.token_iter.next(); } else { panic!("syntax error"); }
		return x;
	}

	fn expr<'f>(&'f mut self) -> Box<Node<'a>> {
		if let Token::Identifier(_) = self.token_iter.peek().unwrap() { } else { return self.test(); }
		let mut x = self.test();
		if let Node::Var(_) = *x {
			if let Token::EqualTo = self.token_iter.peek().unwrap() {
				let t = x;
				self.token_iter.next();
				x  = Box::new(Node::Set{o1: t, o2: self.expr()});
			}
		}
		return x;
	}

	fn test<'f>(&'f mut self) -> Box<Node<'a>> {
		let mut x = self.sum();
		if let Some(Token::Less) = self.token_iter.peek() {
			let t = x;
			self.token_iter.next();
			x = Box::new(Node::Lt{o1: t, o2: self.sum()});
		}
		return x;
	}

	fn sum<'f>(&'f mut self) -> Box<Node<'a>> {
		let mut x = self.term();
		loop {
			match self.token_iter.peek() {
				Some(Token::Plus) => {
					let t = x;
					self.token_iter.next();
					x = Box::new(Node::Add{o1: t, o2: self.term()});
				},

				Some(Token::Minus) => {
					let t = x;
					self.token_iter.next();
					x = Box::new(Node::Sub{o1: t, o2: self.term()});
				},

				_ => { break; }
			}
		}
		return x;
	}

	fn term<'f>(&'f mut self) -> Box<Node<'a>> {
		let (do_next, res) = match self.token_iter.peek().unwrap() {
			Token::Identifier(id) => (true, Box::new(Node::Var(id))),
			Token::Int(i) => (true, Box::new(Node::Cst(*i))),
			_ => (false, self.paren_exp())
		};

		if do_next { self.token_iter.next(); }
		res
	}
}

// code generator

#[derive(Debug)]
enum Inst<'a> {
	Ifetch,
	Istore,
	Ipush,
	Ipop,
	Iadd,
	Isub,
	Ilt,
	Jz,
	Jnz,
	Jmp,
	Halt,
	Val(i32),
	Var(&'a str),
	Hole,
}

#[derive(Debug)]
struct CodeGen<'a> {
	code: Vec<Inst<'a>>,
}

impl<'a> CodeGen<'a> {
	fn new() -> Self {
		CodeGen{code: Vec::new()}
	}

	fn gen(&mut self, x: &'a Node) {
		match x {
			Node::Var(v) => {
				self.code.push(Inst::Ifetch);
				self.code.push(Inst::Var(*v));
			}

			Node::Cst(v) => {
				self.code.push(Inst::Ipush);
				self.code.push(Inst::Val(*v));
			}

			Node::Add{o1, o2} => {
				self.gen(o1);
				self.gen(o2);
				self.code.push(Inst::Iadd);
			}

			Node::Sub{o1, o2} => {
				self.gen(o1);
				self.gen(o2);
				self.code.push(Inst::Isub);
			}

			Node::Lt{o1, o2} => {
				self.gen(o1);
				self.gen(o2);
				self.code.push(Inst::Ilt);
			}

			Node::Set{o1, o2} => {
				self.gen(o2);
				self.code.push(Inst::Istore);
				self.code.push(
					if let Node::Var(v) = **o1 {
						Inst::Var(v)
					} else {
						panic!("error");
					}
				);
			}

			Node::If1{o1, o2} => {
				self.gen(o1);
				self.code.push(Inst::Jz);
				self.code.push(Inst::Hole);
				let j = self.code.len() - 1;
				self.gen(o2);
				self.code[j] = Inst::Val(self.code.len() as i32 - j as i32);
			}

			Node::If2{o1, o2, o3} => {
				self.gen(o1);
				self.code.push(Inst::Jz);
				self.code.push(Inst::Hole);
				let j = self.code.len() - 1;
				self.gen(o2);
				self.code.push(Inst::Jmp);
				self.code.push(Inst::Hole);
				let k = self.code.len() - 1;

				self.code[j] = Inst::Val(self.code.len() as i32 - j as i32);
				self.gen(o3);
				self.code[k] = Inst::Val(self.code.len() as i32 - k as i32);
			}

			Node::While{o1, o2} => {
				let j = self.code.len();
				self.gen(o1);
				self.code.push(Inst::Jz);
				self.code.push(Inst::Hole);
				let k = self.code.len() - 1;
				self.gen(o2);
				self.code.push(Inst::Jmp);
				self.code.push(Inst::Hole);

				// fix
				let l = self.code.len() -1;
				self.code[l] = Inst::Val(j as i32 - l as i32);
				
				let l = self.code.len() - 1;
				self.code[k] = Inst::Val(l as i32 - k as i32);
			}

			Node::Do{o1, o2} => {
				let j = self.code.len();
				self.gen(o1);
				self.gen(o2);
				self.code.push(Inst::Jnz);

				// fix
				self.code.push(Inst::Hole);
				let l = self.code.len() - 1;
				self.code[l] = Inst::Val(j as i32 - l as i32);
			}

			Node::Empty=> {}

			Node::Seq{o1, o2} => {
				self.gen(o1);
				self.gen(o2);
			}

			Node::Expr{o1} => {
				self.gen(o1);
				self.code.push(Inst::Ipop);
			}

			Node::Prog{o1} => {
				self.gen(o1);
				self.code.push(Inst::Halt);
			}
		}
	}
}

fn run(code: Vec<Inst>) {
	let mut stack = Vec::<i32>::new();
	let mut globals = HashMap::<&str, i32>::new();
	let mut pc = 0;

	while pc < code.len() {
		match code[pc] {
			Inst::Ifetch => {
				pc += 1;
				let v = if let Inst::Var(v) = code[pc] { v } else { panic!("error") };
				stack.push(*globals.get(v).unwrap());
			}

			Inst::Istore => {
				pc += 1;
				globals.insert(
					if let Inst::Var(x) = code[pc] {
						x
					} else {
						panic!("error");
					}, *stack.last().unwrap());
				pc += 1;
			}

			Inst::Ipush => {
				pc += 1;
				stack.push(
					if let Inst::Val(x) = code[pc] {
						x
					} else {
						panic!("error");
					}
				);
			}

			Inst::Ipop => {
				pc += 1;
				stack.pop();
			}

			Inst::Iadd => {
				pc += 1;
				let l = stack.len() - 1;
				stack[l - 1] += stack[l];
				stack.pop();
			}

			Inst::Isub => {
				pc += 1;
				let l = stack.len() - 1;
				stack[l - 1] -= stack[l];
				stack.pop();
			}

			Inst::Ilt => {
				pc += 1;
				let l = stack.len() - 1;
				stack[l - 1] = if stack[l - 1] < stack[l] { 1 } else { 0 };
				stack.pop();
			}

			Inst::Jmp => {
				pc += 1;
				pc = pc.wrapping_add(if let Inst::Val(x) = code[pc] { x as usize } else { panic!("error") });
			}

			Inst::Jz => {
				pc += 1;
				if stack.pop().unwrap() == 0 {
					pc = pc.wrapping_add(if let Inst::Val(x) = code[pc] { x as usize } else { panic!("error") });
				} else {
					pc += 1;
				}
			}

			Inst::Jnz => {
				pc += 1;
				if stack.pop().unwrap() != 0 {
					pc = pc.wrapping_add(if let Inst::Val(x) = code[pc] { x as usize } else { panic!("error") });
				} else {
					pc += 1;
				}
			}
			_ => { pc += 1 },
		}
	}

	println!("{:?}", globals);
}

fn main() -> io::Result<()> {
	let mut buf = String::new();
	io::stdin().read_to_string(&mut buf)?;

	println!("{:?}", Tokens::new(&buf).collect::<Vec<Token>>());
	let mut p = Parser::new(&buf);
	let syntax_tree = p.program();
	println!("{:?}", syntax_tree);

	let mut codegen = CodeGen::new();
	codegen.gen(syntax_tree.as_ref());
	println!("{:?}", codegen);

	run(codegen.code);

	Ok(())
}
